const async = require('async'),
    storage = require('./storage.module'),
    externalApi = require('./external-api.module');

function getWeatherFuncFactory(country) {
    return function (callback) {
        externalApi.weather(country.capital).request().then(function (data) {
            storage.setWeather(country, getWeatherFromResponse(data));
            callback();
        }).catch(function (err) {
            console.log('Weather API Error', err);
            callback();
        });
    };
}

function getWeatherFromResponse(res) {
    try {
        var data = JSON.parse(res);
        return {
            "desc": data['weather'][0]['main'],
            "temp": data['main']['temp']
        };
    } catch (err) {
        return {};
    }
}

module.exports = {
    getBulk: function (countries, callback) {
        var parallelFuncs = [];
        countries.forEach(function (country) {
            parallelFuncs.push(getWeatherFuncFactory(country));
        });
        async.parallel(parallelFuncs, function () {
            callback(storage.get());
        });
    }
};