function Storage() {
    var that = this,
        data = [];
    that.add = function (country) {
        this.length = data.push({
            "region": country.region,
            "name": country.name,
            "capital": country.capital
        });
    };
    that.addBulk = function (countries) {
        countries.forEach(function (country) {
            that.add(country);
        });
    };
    that.clear = function () {
        data = [];
        return this;
    };
    that.setWeather = function (country, weather) {
        for (var i = 0, len = data.length; i < len; i++) {
            if (data[i].name == country.name) {
                data[i]['weather'] = weather;
                return true;
            }
        }
        return false;
    }
    this.query = function (query) {
        return data.filter(function (country) {
            return ((country.region.toLowerCase() == query.region)
                && (country.weather.temp >= query.weather.min && country.weather.temp <= query.weather.max)
                && (!query.weather.desc || country.weather.desc.toLowerCase() == query.weather.desc));
        });
    };
    that.get = function () {
        return data;
    };
};
var storage = new Storage();
module.exports = storage;