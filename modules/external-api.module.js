const http = require('http');
var externalApi = new ExternalAPI();

module.exports = externalApi;

function ExternalAPI() {
    var url = '';
    this.countries = function (query) {
        url = 'http://www.restcountries.eu/rest/v2/region/' + (query || '');
        return this;
    };
    this.weather = function (query) {
        url = 'http://api.openweathermap.org/data/2.5/weather?APPID=29c1161f8be593fcb302581053ffe54a&units=metric&q=' + (query || '');
        return this;
    };
    this.request = function () {
        return new Promise(function (resolve, reject) {
            var data = '';
            http.get(url, function (response) {
                response.on('data', function (chunk) {
                    data += chunk;
                });
                response.on('end', function (req) {
                    resolve(data);
                });
                response.on('error', function (err) {
                    reject(err);
                });
            }).end();
        });
    };
}