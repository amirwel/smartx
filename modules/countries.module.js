const externalApi = require('./external-api.module');

module.exports = {
    getByRegion: function (region) {
        return externalApi.countries((region || 'americas').toLowerCase()).request();
    }
};