const express = require('express'),
    bodyParser = require('body-parser'),
    async = require('async'),
    countries = require('./modules/countries.module'),
    weather = require('./modules/weather.module'),
    storage = require('./modules/storage.module'),
    app = express();


/* Middleware */
app.use(bodyParser.json());

/* Define Endpoints */
const API_ROUTE = '/api/v1/';
// countries
app.get(API_ROUTE + 'countries/:region?', function (req, res) {
    countries.getByRegion(req.params.region).then(function (data) {
        storage.clear().addBulk(JSON.parse(data));
        res.status(200).send(storage.get());
    }).catch(function (err) {
        console.log(err);
        res.status(500).send('Failed to create countries list');
    });
});
// weather
app.get(API_ROUTE + 'weather', function (req, res) {
    weather.getBulk(storage.get(), function (updatedStorageData) {
        res.status(200).send(updatedStorageData);
    });
});
// query
app.post(API_ROUTE + 'filter', function (req, res) {
    var rBody = req.body;
    if (rBody.type) {
        var query = { region: rBody.type.toLowerCase() };
        try {
            var filter = rBody.filter;
            query['weather'] = {
                min: (filter.temp.min || Number.NEGATIVE_INFINITY),
                max: (filter.temp.max || Number.POSITIVE_INFINITY),
                desc: filter.weather.type
            };
            res.status(200).send(storage.query(query));
        } catch (err) {
            console.log(err);
            res.status(400).send('Invalid filter');
        }
    } else {
        res.status(400).send('Missing Parameter {type}');
    }
});

/* Start Server */
app.listen(80, function () {
    console.log('server listening on port 80')
});